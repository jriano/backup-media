Backup Media
============

Script:
./backup_media

This script uses the file locations given in
dirs.json

to backup audio, video and images.

To do
-----

- Remove directories if they are empty
- create log by run
- open picture in viewer and mark it as special
- make all a single file/script
- implement prime photos
- implement prime videos
